# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-03-23 02:53+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: outside any tag (error?)
msgid ""
"[[!meta title=\"Verify the ISO image using the command line\"]] [[!inline "
"pages=\"doc/get/signing_key_transition.inline\" raw=\"yes\"]]"
msgstr ""

#. type: Content of: <p>
msgid ""
"You need to have GnuPG installed. GnuPG is the common OpenPGP implementation "
"for Linux: it is installed by default under Debian, Ubuntu, Tails and many "
"other distributions."
msgstr ""

#. type: Content of: <h3>
msgid "Get the Tails signing key"
msgstr ""

#. type: Content of: <p>
msgid "First, <strong>download Tails signing key</strong>:"
msgstr ""

#. type: Content of: outside any tag (error?)
msgid "[[!inline pages=\"lib/download_tails_signing_key\" raw=\"yes\"]]"
msgstr ""

#. type: Content of: <p>
msgid ""
"Open a terminal and <strong>import Tails signing key</strong> with the "
"following commands:"
msgstr ""

#. type: Content of: <pre>
#, no-wrap
msgid ""
"cd [the directory in which you downloaded the key]\n"
"gpg --keyid-format long --import tails-signing.key\n"
msgstr ""

#. type: Content of: <p>
msgid "The output should tell you that the key was imported:"
msgstr ""

#. type: Content of: <pre>
#, no-wrap
msgid ""
"gpg: key DBB802B258ACD84F: public key \"Tails developers (offline long-term identity key) &lt;tails@boum.org&gt;\" imported\n"
"gpg: Total number processed: 1\n"
"gpg:               imported: 1  (RSA: 1)\n"
msgstr ""

#. type: Content of: <p>
msgid ""
"<strong>If you had already imported Tails signing key in the past</strong>, "
"the output should tell you that the key was not changed:"
msgstr ""

#. type: Content of: <pre>
#, no-wrap
msgid ""
"gpg: key DBB802B258ACD84F: \"Tails developers (offline long-term identity key) &lt;tails@boum.org&gt;\" not changed\n"
"gpg: Total number processed: 1\n"
"gpg:              unchanged: 1\n"
msgstr ""

#. type: Content of: <p>
msgid ""
"<strong>If you are shown the following message</strong> at the end of the "
"output:"
msgstr ""

#. type: Content of: <pre>
#, no-wrap
msgid "gpg: no ultimately trusted keys found\n"
msgstr ""

#. type: Content of: <p>
msgid ""
"Analyse the other messages as usual: this extra message doesn't relate to "
"the Tails signing key that you downloaded and usually means that you didn't "
"create an OpenPGP key for yourself yet, which of no importance to verify the "
"ISO image."
msgstr ""

#. type: Content of: <h3>
msgid "Verify the ISO image"
msgstr ""

#. type: Content of: <p>
msgid ""
"Now, <strong>download the cryptographic signature</strong> corresponding to "
"the ISO image you want to verify and save it in the same folder as the ISO "
"image:"
msgstr ""

#. type: Content of: outside any tag (error?)
msgid "[[!inline pages=\"lib/download_stable_i386_iso_sig\" raw=\"yes\"]]"
msgstr ""

#. type: Content of: <p>
msgid ""
"Then, <strong>start the cryptographic verification</strong>, it can take "
"several minutes:"
msgstr ""

#. type: Content of: <p>
msgid "cd [the ISO image directory]"
msgstr ""

#. type: Content of: <p>
msgid "[[!inline pages=\"inc/stable_i386_gpg_verify\" raw=\"yes\"]]"
msgstr ""

#. type: Content of: <p>
msgid ""
"<strong>If the ISO image is correct</strong> the output will tell you that "
"the signature is good:"
msgstr ""

#. type: Content of: <pre>
#, no-wrap
msgid ""
"pg: Signature made Sun 08 Feb 2015 08:17:03 PM UTC\n"
"gpg:                using RSA key 3C83DCB52F699C56\n"
"gpg: Good signature from \"Tails developers (offline long-term identity key) &lt;tails@boum.org&gt;\" [unknown]\n"
"Primary key fingerprint: A490 D0F4 D311 A415 3E2B  B7CA DBB8 02B2 58AC D84F\n"
"     Subkey fingerprint: BA2C 222F 44AC 00ED 9899  3893 98FE C6BC 752A 3DB6\n"
msgstr ""

#. type: Content of: <p>
msgid "or:"
msgstr ""

#. type: Content of: <pre>
#, no-wrap
msgid ""
"pg: Signature made Sun 08 Feb 2015 08:17:03 PM UTC\n"
"gpg:                using RSA key 98FEC6BC752A3DB6\n"
"gpg: Good signature from \"Tails developers (offline long-term identity key) &lt;tails@boum.org&gt;\" [unknown]\n"
"Primary key fingerprint: A490 D0F4 D311 A415 3E2B  B7CA DBB8 02B2 58AC D84F\n"
"     Subkey fingerprint: A509 1F72 C746 BA6B 163D  1C18 3C83 DCB5 2F69 9C56\n"
msgstr ""

#. type: Content of: <p>
msgid "If you see the following warning:"
msgstr ""

#. type: Content of: <pre>
#, no-wrap
msgid ""
"gpg: WARNING: This key is not certified with a trusted signature!\n"
"gpg:          There is no indication that the signature belongs to the owner.\n"
"Primary key fingerprint: A490 D0F4 D311 A415 3E2B  B7CA DBB8 02B2 58AC D84F\n"
msgstr ""

#. type: Content of: <p>
msgid ""
"Then the ISO image is still correct, and valid according to the Tails "
"signing key that you downloaded. This warning is related to the trust that "
"you put in the Tails signing key. See, [[Trusting Tails signing key|doc/get/"
"trusting_tails_signing_key]]. To remove this warning you would have to "
"personally <span class=\"definition\">[[!wikipedia Keysigning desc=\"sign\"]]"
"</span> the Tails signing key with your own key."
msgstr ""

#. type: Content of: <p>
msgid ""
"<strong>If the ISO image is not correct</strong> the output will tell you "
"that the signature is bad:"
msgstr ""

#. type: Content of: <pre>
#, no-wrap
msgid ""
"gpg: Signature made Sat 30 Apr 2015 10:53:23 AM CEST\n"
"gpg:                using RSA key DBB802B258ACD84F\n"
"gpg: BAD signature from \"Tails developers (offline long-term identity key) &lt;tails@boum.org&gt;\"\n"
msgstr ""
