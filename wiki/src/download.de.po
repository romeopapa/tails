# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# Schreppe <EMAIL@ADDRESS>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"POT-Creation-Date: 2015-04-26 10:34+0300\n"
"PO-Revision-Date: 2015-04-26 14:20+0100\n"
"Last-Translator: spriver <spriver@autistici.org>\n"
"Language-Team: Tails language team <tails-l10n@boum.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"

#. type: Content of: outside any tag (error?)
msgid ""
"[[!meta title=\"Download, verify and install\"]] <strong>Tails is [[Free "
"Software|doc/about/license]], you can download it, use it and share it "
"without restriction.</strong>"
msgstr ""
"[[!meta title=\"Herunterladen, prüfen und installieren\"]] <strong>Tails ist "
"[[Freie Software|doc/about/license]], Sie können sie ohne Einschränkung "
"herunterladen, benutzen und verteilen.</strong>"

#. type: Content of: <div>
msgid "[[!toc levels=1]]"
msgstr "[[!toc levels=1]]"

#. type: Content of: <div><div><h1>
msgid "First time user?"
msgstr "Anfänger?"

#. type: Content of: <div><div><div><ul><li>
msgid "If you don't know what a metadata or a man-in-the-middle attack is."
msgstr ""
"Falls Sie nicht wissen, was ein Metadaten- oder ein \"Man-in-the-middle\"-"
"Angriff ist..."

#. type: Content of: <div><div><div><ul><li>
msgid ""
"If you think no-one can eavesdrop on your communications because you are "
"using Tor."
msgstr ""
"Falls Sie denken, niemand könne Ihre Kommunikation abhören, weil Sie Tor "
"verwenden..."

#. type: Content of: <div><div><div><ul><li>
msgid "If you have no notion of how Tails works."
msgstr "Falls Sie keine Vorstellung davon haben, wie Tails funktioniert..."

#. type: Content of: <div><div><div><p>
msgid ""
"<strong>Then, check first the [[about]] and [[warning|doc/about/warning]] "
"pages to make sure that Tails is the right tool for you and that you "
"understand well its limitations.</strong>"
msgstr ""
"<strong>...dann lesen Sie zuerst [[Über Tails|about]] und [[Warnung|doc/"
"about/warning]] durch, damit Sie wissen, ob Tails auch das richtige für Sie "
"ist und Sie dessen Grenzen gut verstehen.</strong>"

#. type: Content of: <div><div><h1>
msgid "Download the ISO image"
msgstr "Laden Sie das ISO-Image herunter"

#. type: Content of: <div><div><div><p>
msgid ""
"You will download Tails in the form of an <span class=\"definition\">[[!"
"wikipedia ISO_image desc=\"ISO image\"]]</span>: a single file that you will "
"later burn on a DVD or install onto a USB stick or SD card."
msgstr ""
"Sie laden Tails als <span class=\"definition\">[[!wikipedia_de ISO_image "
"desc=\"ISO-Image\"]]</span> herunter: Eine einzige Datei, die Sie auf eine "
"DVD brennen, oder auf einen USB-Stick oder eine SD-Karte installieren können."

#. type: Content of: <div><div><div><h2>
msgid "Direct download"
msgstr "Direkter Download"

#. type: Content of: <div><div><div><h3>
msgid "Latest release"
msgstr "Aktuelle Version"

#. type: Content of: <div><div><div><p>
#, fuzzy
#| msgid ""
#| "<a class='download-file' href=[[!inline pages=\"inc/stable_i386_iso_url\" "
#| "raw=\"yes\"]]> Tails [[!inline pages=\"inc/stable_i386_version\" raw=\"yes"
#| "\"]] ISO image</a>"
msgid ""
"<a class='download-file' href=[[!inline pages=\"inc/stable_i386_iso_url\" "
"raw=\"yes\"]]> Tails [[!inline pages=\"inc/stable_i386_version\" raw=\"yes"
"\"]] ISO image <span class=\"download-file-size\">[[!inline pages=\"inc/"
"stable_i386_iso_size\" raw=\"yes\"]]</span></a>"
msgstr ""
"<a class='download-file' href=[[!inline pages=\"inc/stable_i386_iso_url\" "
"raw=\"yes\"]]> Tails [[!inline pages=\"inc/stable_i386_version\" raw=\"yes"
"\"]] ISO-Image</a>"

#. type: Content of: <div><div><div><h3>
msgid "Cryptographic signature"
msgstr "Kryptographische Signatur"

#. type: Content of: <div><div><div>
msgid "[[!inline pages=\"lib/download_stable_i386_iso_sig\" raw=\"yes\"]]"
msgstr "[[!inline pages=\"lib/download_stable_i386_iso_sig\" raw=\"yes\"]]"

#. type: Content of: <div><div><div><p>
msgid ""
"Tails [[transitioned to a new signing key|news/signing_key_transition]] in "
"Tails 1.3.1."
msgstr ""
"Tails ist in Tails 1.3.1 [[zu einer neuen kryptographischen Signatur "
"gewechselt|news/signing_key_transition]]."

#. type: Content of: <div><div><div><p>
msgid ""
"If you're not sure what the cryptographic signature is, please read the part "
"on [[verifying the ISO image|download#verify]]."
msgstr ""
"Falls kryptographische Signaturen neu für Sie sind, informieren Sie sich "
"bitte im Abschnitt [[ISO-Image überprüfen|download#verify]]."

#. type: Content of: <div><div><div><h3>
msgid "SHA256 checksum"
msgstr "SHA256 Prüfsumme"

#. type: Content of: <div><div><div><p>
msgid "[[!inline pages=\"inc/stable_i386_hash\" raw=\"yes\"]]"
msgstr "[[!inline pages=\"inc/stable_i386_hash\" raw=\"yes\"]]"

#. type: Content of: <div><div><div><h2>
msgid "BitTorrent download"
msgstr "BitTorrent Download"

#. type: Content of: <div><div><div><p>
msgid ""
"<a class='download-file' href=[[!inline pages=\"inc/stable_i386_torrent_url"
"\" raw=\"yes\"]]> Tails [[!inline pages=\"inc/stable_i386_version\" raw=\"yes"
"\"]] torrent</a>"
msgstr ""
"<a class='download-file' href=[[!inline pages=\"inc/stable_i386_torrent_url"
"\" raw=\"yes\"]]> Tails [[!inline pages=\"inc/stable_i386_version\" raw=\"yes"
"\"]] Torrent</a>"

#. type: Content of: <div><div><div><p>
msgid ""
"The cryptographic signature of the ISO image is also included in the Torrent."
msgstr ""
"Die kryptographische Signatur des ISO-Images ist auch im Torrent enthalten."

#. type: Content of: <div><div><div><p>
msgid ""
"Additionally, you can verify the <a href=[[!inline pages=\"inc/"
"stable_i386_torrent_sig_url\" raw=\"yes\"]]>signature of the Torrent file</"
"a> itself before downloading the ISO image."
msgstr ""
"Zusätzlich können Sie die <a href=[[!inline pages=\"inc/"
"stable_i386_torrent_sig_url\" raw=\"yes\"]]>Signatur der Torrent Datei</a> "
"selbst vor dem Herunterladen des ISO-Images verifizieren."

#. type: Content of: <div><div><div><h3>
msgid "Seed back!"
msgstr "Seede zurück!"

#. type: Content of: <div><div><div><p>
msgid ""
"Seeding back the image once you have downloaded it is also a nice and easy "
"way of helping spread Tails."
msgstr ""
"Eine gute und einfache Möglichkeit bei der Verbreitung von Tails zu helfen, "
"ist auch das Seeden des Images, sobald Sie es heruntergeladen haben."

#. type: Content of: <div><div><p>
msgid "List of current [[known issues|support/known_issues]] in Tails."
msgstr ""
"Liste von derzeit [[bekannten Problemen|support/known_issues]] in Tails."

#. type: Content of: <div><div><h1>
msgid "Verify the ISO image"
msgstr "ISO-Image überprüfen"

#. type: Content of: <div><div><p>
msgid ""
"It is important to check the <span class=\"definition\">[[!wikipedia "
"Data_integrity desc=\"integrity\"]]</span> of the ISO image you downloaded "
"to make sure that the download went well."
msgstr ""
"Es ist wichtig, die [[!wikipedia_de Integrität_(Informationssicherheit) desc="
"\"Integrität\"]] des heruntergeladenen ISO-Images zu prüfen. Dies stellt "
"sicher, dass der Download gut verlaufen ist."

#. type: Content of: <div><div><div><p>
msgid ""
"Those techniques rely on standard HTTPS and <span class=\"definition\">[[!"
"wikipedia Certificate_authority desc=\"certificate authorities\"]]</span> to "
"make you trust the content of this website.  But, [[as explained on our "
"warning page|doc/about/warning#man-in-the-middle]], you could still be "
"victim of a man-in-the-middle attack while using HTTPS.  On this website as "
"much as on any other of the Internet."
msgstr ""
"Diese Techniken basieren auf normalem HTTPS und <span class=\"definition\">"
"[[!wikipedia_de Zertifizierungsstelle desc=\"Zertifizierungsstellen\"]]</"
"span> um Ihnen Vertrauen in den Inhalt dieser Website zu geben. Trotz der "
"Verwendung von HTTPS können Sie, [[wie auf unserer Warnungsseite erklärt|doc/"
"about/warning#index3h1]], Opfer eines \"Man-in-the-middle\"-Angriffs sein. "
"Auf dieser Webseite, wie auch auf jeder anderen im Internet."

#. type: Content of: <div><div><div><p>
msgid ""
"As a consequence, <strong>they don't provide you with a strong way of "
"checking the ISO image <span class=\"definition\">[[!wikipedia "
"Authentication desc=\"authenticity\"]]</span> and making sure you downloaded "
"a genuine Tails.</strong> In a dedicated section, we will propose you some "
"more advanced techniques to <a href=\"#authenticity-check\">check the "
"authenticity of the ISO image</a>."
msgstr ""
"Daher <strong>stellen diese Techniken keinen sehr sicheren Weg dar, die "
"<span class=\"definition\">[[!wikipedia_de Authentizität desc=\"Echtheit\"]]"
"</span> des ISO-Images zu prüfen und sicherzustellen, dass Sie eine "
"unverfälschte Kopie von Tails heruntergeladen haben.</strong> In einem "
"eigenen Abschnitt erfahren Sie mehr zu den fortgeschrittenen Techniken, um "
"die <a href=\"#authenticity-check\">Echtheit des ISO-Images zu überprüfen</"
"a>."

#. type: Content of: <div><div><p>
msgid ""
"<strong>All Tails ISO image are cryptographically signed by our OpenPGP key."
"</strong> OpenPGP is a standard for data encryption that provides "
"cryptographic privacy and authentication through the use of keys owned by "
"its users.  Checking this signature is the recommended way of checking the "
"ISO image integrity."
msgstr ""
"<strong>Alle Tails ISO-Images sind kryptografisch mit unserem OpenPGP-"
"Schlüssel signiert.</strong> OpenPGP ist ein Standard für "
"Dateiverschlüsselung, der kryptografisch Vertraulichkeit und "
"Authentifizierbarkeit mittels Schlüsseln bietet, die sich im Besitz des "
"jeweiligen Benutzers befinden. Diese Signatur zu überprüfen ist die "
"empfohlene Art die Integrität des ISO-Images zu überprüfen."

#. type: Content of: <div><div><p>
msgid ""
"If you already know how to use an OpenPGP key you can download it straight "
"away:"
msgstr ""
"Wenn Sie bereits wissen, wie man einen OpenPGP-Schlüssel verwendet, können "
"Sie ihn direkt herunterladen:"

#. type: Content of: <div><div>
msgid "[[!inline pages=\"lib/download_tails_signing_key\" raw=\"yes\"]]"
msgstr "[[!inline pages=\"lib/download_tails_signing_key\" raw=\"yes\"]]"

#. type: Content of: <div><div><p>
msgid "Otherwise, read our instructions to check the ISO image integrity:"
msgstr ""
"Anderenfalls lesen Sie unsere Anleitung um die Integrität des ISO-Images zu "
"prüfen:"

#. type: Content of: <div><div><ul><li>
msgid ""
"[[!toggle id=\"verify_the_iso_image_using_gnome\" text=\"Using Linux with "
"Gnome: Ubuntu, Debian, Tails, etc.\"]]"
msgstr ""
"[[!toggle id=\"verify_the_iso_image_using_gnome\" text=\"Unter Linux mit "
"Gnome: Ubuntu, Debian, Tails, etc.\"]]"

#. type: Content of: <div><div><ul><li>
msgid ""
"[[!toggle id=\"verify_the_iso_image_using_the_command_line\" text=\"Using "
"Linux with the command line\"]]"
msgstr ""
"[[!toggle id=\"verify_the_iso_image_using_the_command_line\" text=\"Unter "
"Linux mit der Befehlszeile\"]]"

#. type: Content of: <div><div><ul><li>
msgid ""
"[[!toggle id=\"verify_the_iso_image_using_other_operating_systems\" text="
"\"Using other operating systems\"]]"
msgstr ""
"[[!toggle id=\"verify_the_iso_image_using_other_operating_systems\" text="
"\"Mit einem anderen Betriebssystem\"]]"

#. type: Content of: <div><div>
msgid ""
"[[!toggleable id=\"verify_the_iso_image_using_gnome\" text=\"\"\" <span "
"class=\"hide\">[[!toggle id=\"verify_the_iso_image_using_gnome\" text=\"\"]]"
"</span>"
msgstr ""
"[[!toggleable id=\"verify_the_iso_image_using_gnome\" text=\"\"\" <span "
"class=\"hide\">[[!toggle id=\"verify_the_iso_image_using_gnome\" text=\"\"]]"
"</span>"

#. type: Content of: <div><div><h2>
msgid "Using Linux with Gnome: Ubuntu, Debian, Tails, Fedora, etc."
msgstr "Unter Linux mit Gnome: Ubuntu, Debian, Tails, Fedora, etc."

#. type: Content of: <div><div>
msgid ""
"[[!inline pages=\"doc/get/verify_the_iso_image_using_gnome\" raw=\"yes\"]] "
"\"\"\"]] [[!toggleable id=\"verify_the_iso_image_using_the_command_line\" "
"text=\"\"\" <span class=\"hide\">[[!toggle id="
"\"verify_the_iso_image_using_the_command_line\" text=\"\"]]</span>"
msgstr ""
"[[!inline pages=\"doc/get/verify_the_iso_image_using_gnome.de\" raw=\"yes"
"\"]] \"\"\"]] [[!toggleable id=\"verify_the_iso_image_using_the_command_line"
"\" text=\"\"\" <span class=\"hide\">[[!toggle id="
"\"verify_the_iso_image_using_the_command_line\" text=\"\"]]</span>"

#. type: Content of: <div><div><h2>
msgid "Using Linux with the command line"
msgstr "Unter Linux mit der Kommandozeile"

#. type: Content of: <div><div>
msgid ""
"[[!inline pages=\"doc/get/verify_the_iso_image_using_the_command_line\" raw="
"\"yes\"]] \"\"\"]] [[!toggleable id="
"\"verify_the_iso_image_using_other_operating_systems\" text=\"\"\" <span "
"class=\"hide\">[[!toggle id="
"\"verify_the_iso_image_using_other_operating_systems\" text=\"\"]]</span>"
msgstr ""
"[[!inline pages=\"doc/get/verify_the_iso_image_using_the_command_line.de\" "
"raw=\"yes\"]] \"\"\"]] [[!toggleable id="
"\"verify_the_iso_image_using_other_operating_systems\" text=\"\"\" <span "
"class=\"hide\">[[!toggle id="
"\"verify_the_iso_image_using_other_operating_systems\" text=\"\"]]</span>"

#. type: Content of: <div><div><h2>
msgid "Using other operating systems"
msgstr "Mit einem anderen Betriebssystem"

#. type: Content of: <div><div>
msgid ""
"[[!inline pages=\"doc/get/verify_the_iso_image_using_other_operating_systems"
"\" raw=\"yes\"]] \"\"\"]]"
msgstr ""
"[[!inline pages=\"doc/get/verify_the_iso_image_using_other_operating_systems."
"de\" raw=\"yes\"]] \"\"\"]]"

#. type: Content of: <div><div><h2>
msgid ""
"<a id=\"authenticity-check\"></a>So how can I better check the ISO image "
"authenticity?"
msgstr ""
"<a id=\"authenticity-check\"></a> Wie kann ich die Echtheit des ISO-Images "
"besser überprüfen?"

#. type: Content of: <div><div><p>
msgid ""
"The Tails signing key that you downloaded from this website could be a fake "
"one if you were victim of a [[man-in-the-middle attack|doc/about/warning#man-"
"in-the-middle]]."
msgstr ""
"Der Tails signing key, den Sie von dieser Webseite herunter geladen haben, "
"könnte gefälscht sein, falls Sie das Opfer eines sogenannten [[\"Man-in-the-"
"middle\"-Angriffs|doc/about/warning#man-in-the-middle]] waren."

#. type: Content of: <div><div><p>
msgid ""
"Finding a way of trusting better Tails signing key would allow you to "
"authenticate better the ISO image you downloaded. The following page will "
"give you hints on how to increase the trust you can put in the Tails signing "
"key you downloaded:"
msgstr ""
"Eine Möglichkeit zu finden, dem Tails signing key mehr Vertrauen zukommen zu "
"lassen, würde ebenfalls eine bessere Überprüfung des heruntergeladenen ISO-"
"Images ermöglichen. Die folgende Seite wird Ihnen Hinweise darüber geben, "
"wie Sie die Vertrauenswürdigkeit des heruntergeladenen Tails signing key "
"erhöhen können:"

#. type: Content of: <div><div><ul><li>
msgid "[[Trusting Tails signing key|doc/get/trusting_tails_signing_key]]"
msgstr "[[Dem Tails signing key vertrauen|doc/get/trusting_tails_signing_key]]"

#. type: Content of: <div><div><h1>
msgid "Stay tuned"
msgstr "Auf dem Laufenden bleiben"

#. type: Content of: <div><div><div><p>
msgid ""
"It's very important to keep your version of Tails up-to-date, otherwise your "
"system will be vulnerable to numerous security holes."
msgstr ""
"Es ist sehr wichtig, Ihre Tails Version aktuell zu halten. Sonst wird Ihr "
"System durch zahlreiche Sicherheitslücken angreifbar sein."

#. type: Content of: <div><div><p>
msgid ""
"To be notified of new versions and important project news, follow our [[news "
"feed|news]] or subscribe to our <a href=\"https://mailman.boum.org/listinfo/"
"amnesia-news\">news mailing list</a>:"
msgstr ""
"Um über neue Versionen und wichtige Neuigkeiten informiert zu werden, folgen "
"Sie unserem  [[Newsfeed|news]], oder abonnieren unsere <a href=\"https://"
"mailman.boum.org/listinfo/amnesia-news\">news-Mailingliste</a>:"

#. type: Content of: <div><div><form>
msgid ""
"<input class=\"text\" name=\"email\" value=\"\"/> <input class=\"button\" "
"type=\"submit\" value=\"Subscribe\"/>"
msgstr ""
"<input class=\"text\" name=\"email\" value=\"\"/> <input class=\"button\" "
"type=\"submit\" value=\"Abonnieren\"/>"

#. type: Content of: <div><div><h1>
msgid "Installation"
msgstr "Installation"

#. type: Content of: <div><div><div><p>
msgid ""
"Read the [[First steps|doc/first_steps]] section of our documentation to "
"learn how to burn the ISO image on a DVD or install it onto a USB stick or "
"SD card."
msgstr ""
"Lesen Sie den Abschnitt zu den [[Ersten Schritten mit Tails|doc/"
"first_steps]] aus unserer Dokumentation, um zu erfahren, wie Sie das ISO-"
"Image auf eine DVD brennen, oder auf einen USB-Stick oder eine SD-Karte "
"installieren können."

#~ msgid "Set up a web mirror"
#~ msgstr "Einen Spiegelserver einrichten"

#~ msgid ""
#~ "If you're running a web server, you're most welcome to help us spread "
#~ "Tails by [[setting up a web mirror|contribute/how/mirror]]."
#~ msgstr ""
#~ "Sollten Sie einen Web-Server betreiben, sind Sie herzlich eingeladen, uns "
#~ "bei der Verbreitung von Tails durch das [[Einrichten eines Spiegelservers|"
#~ "contribute/how/mirror]] zu unterstützen."

#~ msgid ""
#~ "New versions are announced on our <a href='https://mailman.boum.org/"
#~ "listinfo/amnesia-news'>news mailing-list</a>. Drop your email address "
#~ "into this box, then hit the button to subscribe:"
#~ msgstr ""
#~ "Neue Versionen werden auf unserer <a href='https://mailman.boum.org/"
#~ "listinfo/amnesia-news'>News-Mailingliste</a> bekannt gegeben. Geben Sie "
#~ "Ihre E-Mail-Adresse in das Feld ein und drücken sie dann den Button um "
#~ "sich einzutragen:"

#~ msgid ""
#~ "There also are <a href='/torrents/rss/index.rss'>RSS</a> and <a href='/"
#~ "torrents/rss/index.atom'>Atom</a> feeds that announce new available "
#~ "BitTorrent files."
#~ msgstr ""
#~ "Es gibt auch <a href='/torrents/rss/index.rss'>RSS</a> und <a href='/"
#~ "torrents/rss/index.atom'>Atom</a> Feeds, die neu verfügbare BitTorrent "
#~ "Dateien bekannt geben."

#~ msgid ""
#~ "Refer to our [[security announcements|/security]] feed for more detailed "
#~ "information about the security holes affecting Tails.  Furthermore you "
#~ "will be automatically notified of the security holes affecting the "
#~ "version you are using at the startup of a new Tails session."
#~ msgstr ""
#~ "Schauen Sie in unseren [[security announcements|/security]] Feed, um "
#~ "weitere Informationen über die Tails betreffenden Sicherheitslücken zu "
#~ "erfahren. Außerdem werden Sie beim Start von Tails automatisch über "
#~ "Sicherheitslücken Ihrer Version informiert."
